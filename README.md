# PXE Boot Server Setup Scripts on Arch Linux

PXE boot server (including DHCP/DNS/NAT) setup scripts for diskless workstation.

## Description

This set of bash scripts (referred to as "the scripts") will install and configure dnsmasq, enabling DHCP and caching DNS functions, and configure firewall to set up NAT. The installation and configuration can be customized by simply changing the settings in `setup_pxeserver.sh`. Root filesystem of the diskless workstation can be served by NFS or NBD. PXE boot program can be either GRUB or PXELINUX.

## Usage

1. Copy all files (in either `nfs` or `nbd`) to the computer.
2. Edit `setup_pxeserver.sh`.
3. Run `setup_pxeserver.sh`.

## License

This project is released under GNU General Public License version 2.

## Contained Software

[Syslinux](https://www.syslinux.org/) is used as the Network Bootstrap Program, which can be downloaded from [here](https://www.kernel.org/pub/linux/utils/boot/syslinux/).
