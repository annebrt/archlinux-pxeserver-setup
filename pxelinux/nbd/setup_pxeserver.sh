#!/bin/bash

# Custom Settings
# Note: If 'static_ip' is not null, MAC/IP mappings will be read from /etc/ethers.
ifname_int=ens1
ifname_ext=ens2
local_domain=localdomain
static_ip=
dhcp_ip_start=192.168.0.11
dhcp_ip_end=192.168.0.19
boot_img_path=/var/lib/tftpboot
nbd_name=arch
nbd_device=/dev/dlws/root

if [ $EUID -ne 0 ]; then
  echo "Run this script as root please."
  exit
fi

function check_and_install
{
    inst_pkgs=
    for pkg_name; do
      if ! pacman -Qs "^${pkg_name}" >/dev/null 2>&1; then
        inst_pkgs="${inst_pkgs} ${pkg_name}"
      fi
    done
    if [ -n "${inst_pkgs}" ]; then
      pacman --noconfirm -S ${inst_pkgs}
    fi
}

check_and_install dnsmasq nbd

mkdir -p ${boot_img_path}

tar -C ${boot_img_path} -xf tftpboot.tar.xz

[ -f ${nbd_device} ] && chown -R nbd:nbd $(dirname ${nbd_device})

[ -n "${static_ip}" ] && dhcp_ip_end=static
sed "s/IFNAME/${ifname_int}/; s/IPSTART/${dhcp_ip_start}/; s/IPEND/${dhcp_ip_end}/; s#TFTPBOOT#${boot_img_path}#; s/LOCALDOMAIN/${local_domain}/; ${static_ip:+/^dhcp-range=/ i read-ethers}" dnsmasq.conf > /etc/dnsmasq.conf
sed "s/NBDNAME/${nbd_name}/; s#BLOCKDEV#${nbd_device}#" nbd-server.config > /etc/nbd-server/config

if [ -e /etc/selinux/config ]; then
  semanage fcontext -a -t tftpdir_t "${boot_img_path}(/.*)?"
  restorecon -R -v ${boot_img_path}
fi

firewall-cmd --permanent --zone=internal --add-service=dns
firewall-cmd --permanent --zone=internal --add-service=dhcp
firewall-cmd --permanent --zone=internal --add-service=tftp
firewall-cmd --permanent --zone=internal --add-service=nbd
firewall-cmd --permanent --zone=external --change-interface=${ifname_ext}
firewall-cmd --permanent --zone=internal --change-interface=${ifname_int}
firewall-cmd --permanent --new-policy=nat_int_to_ext
firewall-cmd --permanent --policy=nat_int_to_ext --add-ingress-zone=internal
firewall-cmd --permanent --policy=nat_int_to_ext --add-egress-zone=external
firewall-cmd --permanent --policy=nat_int_to_ext --set-target=ACCEPT
firewall-cmd --reload

systemctl enable dnsmasq
systemctl restart dnsmasq
systemctl enable nbd
systemctl restart nbd
